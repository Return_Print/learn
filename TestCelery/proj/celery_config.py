#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    : celery_config.py
@Time    : 2021/11/9 22:27
@Author  : Qi
@Email   : 18821723039@163.com
@Software: PyCharm
"""

from __future__ import absolute_import, unicode_literals

from celery import Celery

app = Celery('proj',
             broker="redis://127.0.0.1/3",
             backend="redis://127.0.0.1/4",
             include=['proj.tasks'])

# Optional configuration, see the application user guide.
app.conf.update(
    result_expires=3600,
)

if __name__ == '__main__':
    app.start()
# TestCelery 目录下执行 项目文件.配置文件
# celery -A proj.celery_config worker -l info
