#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    : tasks.py
@Time    : 2021/11/9 22:27
@Author  : Qi
@Email   : 18821723039@163.com
@Software: PyCharm
"""

from __future__ import absolute_import, unicode_literals

from .celery_config import app


@app.task
def add(x, y):
    return x + y


@app.task
def mul(x, y):
    return x * y


@app.task
def xsum(numbers):
    # return sum(numbers)
    return numbers