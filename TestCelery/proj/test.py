#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    : test.py
@Time    : 2021/11/9 22:45
@Author  : Qi
@Email   : 18821723039@163.com
@Software: PyCharm
"""
from celery import group, chord

from proj.tasks import mul, add, xsum

# res = mul.delay(1, 2)
# print(res)
# print(res.get())

# a = add.s(2, 2)
# print(a.delay().get())
#
#
# res = chord((add.s(i, i) for i in range(10)), xsum.s())().get()
# print(res)

res = (group(add.s(i, i) for i in range(10)) | xsum.s() | mul.s(4))().get()

print(res)