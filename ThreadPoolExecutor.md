```python
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    : with_threadpool.py
@Time    : 2021/7/20 00:14
@Author  : Qi
@Email   : 18821723039@163.com
@Software: PyCharm
"""
import time
from concurrent.futures import ThreadPoolExecutor

dict1 = {}


def task(page):
    time.sleep(0.01)
    return {f"page_{page}": page}


ss = time.time()
with ThreadPoolExecutor(max_workers=4, thread_name_prefix='test') as pool:
    # thread_name_prefix 给线程起别名
    results = pool.map(task, range(1000))

    for i in results:
        dict1.update(i)
ee = time.time()
print(ee - ss)
print(dict1)

dict2 = {}
for i in range(1000):
    dict2.update(task(i))

print(dict2)
ss1 = time.time()
print(ss1 - ee)
```

```python
# 获取当前线程名称
from threading import current_thread

thread_name = current_thread().getName()


```