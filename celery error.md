### celery error
-  AttributeError: 'list' object has no attribute 'decode'

```python
AttributeError: 'list' object has no attribute 'decode'
  File "celery/worker/worker.py", line 208, in start
    self.blueprint.start(self)
  File "celery/bootsteps.py", line 119, in start
    step.start(parent)
  File "celery/bootsteps.py", line 369, in start
    return self.obj.start()
  File "celery/worker/consumer/consumer.py", line 318, in start
    blueprint.start(self)
  File "celery/bootsteps.py", line 119, in start
    step.start(parent)
  File "celery/worker/consumer/consumer.py", line 599, in start
    c.loop(*c.loop_args())
  File "celery/worker/loops.py", line 113, in synloop
    connection.drain_events(timeout=2.0)
  File "kombu/connection.py", line 324, in drain_events
    return self.transport.drain_events(self.connection, **kwargs)
  File "kombu/transport/virtual/base.py", line 963, in drain_events
    get(self._deliver, timeout=timeout)
  File "kombu/transport/redis.py", line 369, in get
    self._register_BRPOP(channel)
  File "kombu/transport/redis.py", line 310, in _register_BRPOP
    channel._brpop_start()
  File "kombu/transport/redis.py", line 727, in _brpop_start
    self.client.connection.send_command('BRPOP', *keys)
  File "redis/connection.py", line 726, in send_command
    check_health=kwargs.get('check_health', True))
  File "redis/connection.py", line 701, in send_packed_command
    self.check_health()
  File "redis/connection.py", line 685, in check_health
    if nativestr(self.read_response()) != 'PONG':
  File "redis/_compat.py", line 168, in nativestr
    return x if isinstance(x, str) else x.decode('utf-8', 'replace')

```

[同样的问题1](http://www.lovres.com/%E5%86%85%E5%8A%9F%E5%BF%83%E6%B3%95/Python%E5%BA%93%E7%9A%84%E4%BD%BF%E7%94%A8/Celery%E7%9A%84%E4%BD%BF%E7%94%A8.html)
[同样的问题2](https://www.vuln.cn/8647)

