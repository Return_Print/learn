#####IDEA 或者GoLand/PyCharm 控制台显示的太多

1. ![image-20210801102337035](idea_setting/image-20210801102337035.png)

解决办法:
	快捷键输入 : Cmd + Shift + A (mac)  Ctrl + Shift + A (win)
	输入  'registry'  不是 'register'
	![image-20210801102732642](idea_setting/image-20210801102732642.png)
打开一个窗口，找到run.process.with.pty,新版的是go.run.process.with.pty这个属性，然后取消勾选，就可以啦。

![image-20210801103339150](idea_setting/image-20210801103339150.png)
