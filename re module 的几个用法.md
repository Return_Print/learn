[正则官方文档](https://docs.python.org/zh-cn/3/library/re.html)

```python
import re
pattern = re.compile(
        r"""
            (?P<name>[\w\+]+)://
            (?:
                (?P<username>[^:/]*)
                (?::(?P<password>[^@]*))?
            @)?
            (?:
                (?:
                    \[(?P<ipv6host>[^/\?]+)\] |
                    (?P<ipv4host>[^/:\?]+)
                )?
                (?::(?P<port>[^/\?]*))?
            )?
            (?:/(?P<database>[^\?]*))?
            (?:\?(?P<query>.*))?
            """,
        re.X,
    )

m = pattern.match("mysql://root:***@localhost:3306/py_test")
components = m.groupdict()
print(components)
"""
{'name': 'mysql', 'username': 'root', 'password': '***', 'ipv6host': None, 'ipv4host': 'localhost', 'port': '3306', 'database': 'py_test', 'query': None}

"""
```



|     字符     | 功能                                                         |
| :----------: | :----------------------------------------------------------- |
|      \|      | 匹配左右任意一个表达式                                       |
|     (ab)     | 将括号中字符作为一个分组                                     |
|    `\num`    | 引用分组num匹配到的字符串                                    |
| `(?P<name>)` | 分组起别名                                                   |
|  (?P=name)   | 引用别名为name分组匹配到的字符串                             |
|    (?:…)     | 正则括号的非捕获版本。 匹配在括号内的任何正则表达式，但该分组所匹配的子字符串 *不能* 在执行匹配后被获取或是之后在模式中被引用 |