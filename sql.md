1. coalesce()的用法
   coalesce()解释：返回参数中的第一个非空表达式（从左向右）； 
   鉴于在 [mysql](http://www.2cto.com/database/MySQL/)中没有nvl()函数, 我们用coalesce()来代替。
   coalesce相比nvl优点是，coalesce中参数可以有多个，而nvl()中参数就只有两个。
   当然，在oracle中也可以使用 case when....then....else......end

```sql
select coalesce(a,b,c);  
-- 如果a==null,则选择b；如果b==null,则选择c；如果a!=null,则选择a；如果a b c 都为null ，则返回为null（没意义）。
```

2. nvl  函数

   是orcale 数据库特有的函数

   ```sql
   select nvl(a, b)
   -- 如果a 为空 则返回b, 否则返回 a
   ```

3. IFNULL  函数 和 nvl 功能相同, 但是ifnull 是MySQL数据库的特有函数

4. **decode**(字段或字段的运算，值1，值2，值3）

   ​    这个函数运行的结果是，当字段或字段的运算的值等于值1时，该函数返回值2，否则返回值3
    当然值1，值2，值3也可以是表达式，这个函数使得某些sql语句简单了许多，

5. null 不能和字符串作比较
