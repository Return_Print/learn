SQLALCHEMY 创建链接数据库

```python
from sqlalchemy import Column, CHAR, INTEGER
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import literal

Base = declarative_base()
engine = create_engine("mysql://root:password@host:3306/py_test", echo=True)
Session = sessionmaker(engine)
session = Session()
```

一. Sqlalchemy 给查询的结果集添加默认的字段

```python
from sqlalchemy import literal

teachers = session.query(Teacher.tec_name.label("name"), literal("1").label("num"))
# 输出结果添加了 num = 1
# select f.name, "1" as num  from teacher as f
```

二. sqlalchemy 左连接

```python
res = session.query(Students.age, Classes.name).outerjoin(Classes, Students.cls_id == Classes.id).all()
# query(查询对象.字段)
# select s.age, c.name from students s left join classes c on s.cls_id = c.id
```

三. union 纵向合并两张表

```python
class User(Base):
    __tablename__ = "user"

    id = Column(CHAR(20), primary_key=True)
    name = Column(CHAR(20))
    age = Column(INTEGER)

    def __repr__(self):
        return f"name {self.name}, age {self.age}"


class Teacher(Base):
    __tablename__ = "teacher"

    id = Column(CHAR(20), primary_key=True)
    tec_name = Column(CHAR(20))
    tec_age = Column(INTEGER)


# 两张表字段可以对其
user = session.query(User.name, User.age)
teachers = session.query(Teacher.tec_name.label("name"), Teacher.tec_age.label("age"))
res = user.union(teachers)  # 纵向链接两张表

# 如果下面的表只有一个字段  后面可以用None 补齐
teachers = session.query(Teacher.tec_name.label("name"), None)
from sqlalchemy import literal  # 给字段添加默认值

teachers = session.query(Teacher.tec_name.label("name"), literal("3").label("age"))
```

四 group_by 分组

```python
res = session.query(Goods.cate_name, Goods.brand_name).group_by(Goods.cate_name, Goods.brand_name)

from sqlalchemy import func  # 统计计算需要导入func函数

# 求分组统计
res = session.query(func.max(Goods.price)).group_by(Goods.cate_name, Goods.brand_name)

# 分组求统计之后的某所有最大的数据
"""
1. 将子查询按照传统的方式写好查询代码，然后在query对象后面执行subquery方法，将这个查询变成一个子查询。
2. 在子查询中，将以后需要用到的字段通过label方法，取个别名。
3. 在父查询中，如果想要使用子查询的字段，那么可以通过子查询的返回值上的c属性拿到（c=Column）。
"""
Goods.cate_name.label("cate_name")).group_by(Goods.cate_name).subquery()
res = session.query(Goods).filter(Goods.price == submaxprice.c.price, Goods.cate_name == submaxprice.c.cate_name).all()




```

五 .使用别名（aliased）

SQLAlchemy 使用 `aliased()` 方法表示别名，当我们需要把同一张表连接多次的时候，常常需要用到别名

```python
from sqlalchemy.orm import aliased

# 把 Address 表分别设置别名
adalias1 = aliased(Address)
adalias2 = aliased(Address)

for username, email1, email2 in
        session.query(User.name, adalias1.email_address, adalias2.email_address).
                join(adalias1, User.addresses).
                join(adalias2, User.addresses).
                filter(adalias1.email_address == 'jack@google.com').
                filter(adalias2.email_address == 'j25@yahoo.com'):
    print(username, email1, email2)

# 执行结果
jack
jack @ google.com
j25 @ yahoo.com
```

六. 左连接

```python
# query参数里面标明要查询的字段, 或者放两个表的对像  outerjoin第一个参数是 左边链接的表, 后边是链接条件
res = session.query(Students.age, Classes.name).outerjoin(Classes, Students.cls_id == Classes.id)

```

七. CASE 用法

```python
# case [ (), else_=""  ]  当年龄是18时 赋值2, 不是十八返回 else_=Students.age
res = session.query(
    case(
        [
            (Students.age == 18, 2)
        ],
        else_=Students.age
    ).label("年龄"),
    Students.cls_id,
    func.nvl(Students.birthday, "null").label("birthday")
).all()
```

八. 窗口函数

**row_number() OVER (PARTITION BY COL1 ORDER BY COL2)** 表示根据COL1分组，在分组内部根据 COL2排序，而此函数计算的值就表示每组内部排序后的顺序编号（组内连续的唯一的)

```python
根据
cls_id
分组, 组内根据age排序
pub
组内连续的唯一的排序序号
res = session.query(
    func.row_number().over(
        partition_by=Students.cls_id, order_by=desc(Students.age)
    ).label("pub"),
    Students.cls_id,
    func.nvl(Students.birthday, "null").label("birthday")
).all()
```

九. null 值判断

```python
from sqlalchemy import func

db_type = session.bind.name  # 获取链接数据库的类型
print(db_type)  # mysql 

func.nvl(a, b)  # orcale  a为空结果则是b, 否则是a
func.IFNULL()  # mysql 数据库  同nvl
func.coalesce(a, b, c)  # 通用函数 可以有多个参数
```

十. 打印原始sql语句

```python

from sqlalchemy.dialects import mysql

sql = query.statement.compile(dialect=mysql.dialect(), compile_kwargs={"literal_binds": True})
```

十一. pandas 读取 query对象

```python
# 读取query 对象
df = pd.read_sql_query(query.statement, query.session.bind)
# 读取sql 原始语句
df = pd.read_sql_query(sql, session.bind)
```